import { Component,OnInit } from '@angular/core';
import Books from './bookdata.json';
import Foods from './fooddata.json';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'My-second-app';
  Books: any = (Books as any).default;

  // constructor(){}
  // ngOnInit(){
   
  //    console.log(Books);
  // }
}
