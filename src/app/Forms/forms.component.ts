import { Component,OnInit } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import data from './data.json'
@Component({
    selector: 'app-forms',
    templateUrl: './forms.component.html',

   // styleUrls: ['./forms.component.css']
  })
  export class FormsComponent implements OnInit  {
    title_1 = 'FormComponent';
    
  
    Data = data;
    selectedValue: any;
    
    ddlProducts = ['Foods', 'Books'];
    constructor(public fb: FormBuilder) { }

    ngOnInit() {
      this.selectedValue=this.ddlProducts[0]
     
   }
  ddlProductForm = this.fb.group({
    name: ['']
  })

  onSubmit() {
   
    this.selectedValue= (this.ddlProductForm.value["name"]);
    }
  }
  